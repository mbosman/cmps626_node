function createLabels(datearray) {
  var labels = [];
  datearray.forEach((element) => {
    labels.push(element.datetime);
  });
  return labels;
}

function createarray(inarray) {
  var array = [];
  inarray.forEach((element) => {
    array.push(element.value);
  });
  return array;
}
function getData() {
  var obj = $("#myTable tbody tr")
    .map(function () {
      var $row = $(this);
      var t1 = $row.find(":nth-child(1)").text();
      var t2 = $row.find(":nth-child(4)").text();
      var t3 = $row.find(":nth-child(5)").text();
      return {
        value: $row.find(":nth-child(1)").text(),
        datetime:
          $row.find(":nth-child(4)").text() +
          " " +
          $row.find(":nth-child(5)").text(),
      };
    })
    .get();
  return obj;
}

$(document).ready(function () {
  var data = getData();
  var labels = createLabels(data);
  var values = createarray(data);
  var ctx = document.getElementById("mychart");
  var myChart = new Chart(ctx, {
    type: "line",
    data: {
      labels: labels,
      datasets: [
        {
          label: "Sensor",
          data: values,
          backgroundColor: ["rgba(255, 3, 4, 0)"],
          borderWidth: 2,
          borderColor: "rgba(255,255,255,1)",
        },
      ],
    },
    //   options: {
    //     scales: {
    //       yAxes: [
    //         {
    //           ticks: {
    //             beginAtZero: true,
    //           },
    //         },
    //       ],
    //     },
    //   },
  });
  $("#myTable").DataTable({
    order: [
      [3, "desc"],
      [4, "desc"],
    ],
  });
});
