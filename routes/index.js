var express = require("express");
var router = express.Router();
const monk = require("monk");
const { v4: uuidv4 } = require("uuid");

//? Setup Variables -------------------------------------------------

var db = monk(process.env.MONK_URI);
db.then(() => {
  console.log("Connected correctly to Database");
});
const dataset = db.get(process.env.DATABASE_NAME, { castIds: true });
const apikeys = db.get(process.env.API_KEYS);

//? ----------------------------------------------------------------

//! GET "/" ------------------------------------------------------

router.get("/", async function (req, res, next) {
  await dataset.find({}).then((docs) => {
    res.render("index", { title: "Express", data: docs });
  });
});

//! -----------------------------------------------------------------------

//! POST "/" ------------------------------------------------------

router.post("/", async function (req, res, next) {
  var datetimes = req.body;
  console.log(datetimes);
  await dataset
    .find({
      date: {
        $gte: datetimes.datefrom,
        $lte: datetimes.dateto,
      },
      time: {
        $gte: datetimes.timefrom,
        $lte: datetimes.timeto,
      },
    })
    .then((docs) => {
      console.log(docs);
      res.render("index", { title: "Express", data: docs });
    });
});

//! -----------------------------------------------------------------------

//! POST New device  ------------------------------------------------------

router.post("/create/device", async function (req, res) {
  var info = req.body;
  if (info.deviceName == undefined || info.deviceOwner == undefined) {
    return res.status(400).send("");
  }
  apikeys
    .find({ "device.deviceName": info.deviceName })
    .then(async (devices) => {
      if (devices.length == 0) {
        var newdevice = await apikeys.insert({ key: uuidv4(), device: info });
        console.log(newdevice);
        return res.send(newdevice);
      } else {
        console.log(devices[0].key);
        return res.send("Device Exists, this is the key " + devices[0].key);
      }
    });
});

//! ------------------------------------------------------

//! POST new Value with specific Key ---------------------------------------

router.post("/value/:apiKey", async function (req, res, next) {
  await apikeys.find({ key: req.params.apiKey }).then(async (device) => {
    if (device.length > 0) {
      console.log(device[0].device);
      var incomingdata = req.body;
      var datetime = new Date().toISOString();
      incomingdata.date = datetime.split("T")[0];
      incomingdata.time = datetime.split("T")[1].split("Z")[0];
      incomingdata.device = device[0].device;
      var result = await dataset.insert(incomingdata);
      return res.status(201).send(result);
    } else {
      return res.status(400).send("No device found");
    }
  });
});

//! ------------------------------------------------------

module.exports = router;
